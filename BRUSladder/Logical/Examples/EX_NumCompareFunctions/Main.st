
PROGRAM _INIT
	//Corresponds to ms
	pulseTimer.PRE := 3000;
	resTimer.PRE := 10000;
	tonfTimer.PRE := 10000;
	ctdfCounter.PRE := -10000;
	ctufCounter.PRE := 10000;
	
	numArray[0] := 1;
	numArray[1] := 9;
	numArray[2] := 6;
	numArray[3] := 6;
	numArray[4] := 7;
	numArray[5] := 6;
	numArray[6] := 1;
	numArray[7] := 9;
	numArray[8] := 6;
	numArray[9] := 6;
END_PROGRAM

PROGRAM _CYCLIC
	
	
	
	//	AVE takes in an enable, address to an Array, address to a real to store the average, 
	//	address to a control structure type, the length, and the type of the num (with 'lhAVE_DATATYPE' before it)
	//	After it finishes, the 'numAve' will be populated with the average
	AVE(TRUE, ADR(numArray), ADR(numAve), ADR(controlVar) , 10, lhAVE_DATATYPE_INT);
	
	
	adrNumArray := ADR(numArray);
	
	
	//	BTCLR sets a bit in the binary expression of num to 0 as defined by the value of bitTooClear (starting at 0)
	BITCLR(bitClrEnable, ADR(num), bitToClear);
	//	BTCLR sets a bit in the binary expression of num to 1 as defined by the value of bitTooClear (starting at 0)
	BITSET(bitSetEnable, ADR(num), bitToSet);
	
	
	
	
	
	
	//	BITTST takes in a DINT and through the bitNum, analyzes individual binary elemets of the DINT
	// 	with the polarity set to FALSE, the bittsResult will equal the 1 or the 0 in the set bit
	//	with the polarity set to TRUE, the bittsResult will be the opposite of the 1 or 0 in the set bit
	bittsResult := BITTST(bittstEnable, testVar, bitNum, polarity);
	
	
	
	
	//	SQPB with lhSWPB_MODE_WORD set will take the second half of a DINT, binary wise, and use it
	//	to form the first half of the swapped num and also use the first half of a DINT and use it to
	// 	to form the second half of the swapped num (1st + 2nd into 3rd + 4th / 3rd + 4th into 1st + 2nd)
	//
	//          	0000_0000_0000_0000_0000_0000_0000_0000
	//	    		   1st  	 2nd       3rd       4th
	//
	// 	WITH lhSWPB_MODE_HIGHLOW set, if each set of 2 sets of 4 in binary from left to right were numbered (as shown above_
	//	lhSWPB_MODE_HIGHLOW takes the 1st from the source and places it in the 2nd of the swapped, and 
	// 	does the rest as follows, 2nd into 1st, 3rd into 4th, and 4th into 3rd.
	// 
	// WITH lhSWPB_MODE_REVERSE set, it goes 4 into 1, 3 into 2, 2 into 3, and 1 into 4
	swpbResult 	:= SWPB(swpbEnable, ADR(sourceNum), lhSWPB_MODE_REVERSE, ADR(swappedNum));
	
	
	
	
	
	//	EQU returns TRUE if inputs are equal (UDINT)
	ansEQUtrue 		:= EQU(TRUE, 11, 11);
	ansEQUfalse 	:= EQU(TRUE, 10, 20);
	//	EQUf returns TRUE if inputs are equal (REAL)
	ansEQUftrue 	:= EQUf(TRUE, 101.112, 101.112);
	ansEQUffalse 	:= EQUf(TRUE, 99.123, 98.687);
	
	
	
	
	
	
	
	//	NEQ returns TRUE if inputs are not equal (UDINT)
	ansNEQtrue 		:= NEQ(TRUE, 11, 20);
	ansNEQfalse 	:= NEQ(TRUE, 10, 10);
	//	NEQf returns TRUE if inputs are not equal (REAL)
	ansNEQftrue 	:= NEQf(TRUE, 101.112, 101.113);
	ansNEQffalse 	:= NEQf(TRUE, 99.123, 99.123);
	
	
	
	
	
	
	
	//	GEQ compares the first UDINT (or smaller num) with the second returning postitve if the first is GREATER OR EQUAL
	ansGEQfalse 	:= GEQ(TRUE, 10, 20);
	ansGEQtrue 		:= GEQ(TRUE, 20, 10);
	//	GEQf compares the first REAL (or smaller num) with the second returning postitve if the first is GREATER OR EQUAL
	ansGEQffalse 	:= GEQf(TRUE, 10.123, 20.987);
	ansGEQftrue 	:= GEQf(TRUE, 20.2234, 10.9898908923883888830021);
	
	
	
	
	
	
	
	//	GRT compares the first UDINT (or smaller num) with the second returning postitve if the first is GREATER
	ansGRTfalse 	:= GRT(TRUE, 10, 20);
	ansGRTtrue 		:= GRT(TRUE, 20, 10);
	//	GRTf compares the first REAL (or smaller num) with the second returning postitve if the first is GREATER
	ansGRTffalse 	:= GRTf(TRUE, 10.166762776, 20.0000900009000917);
	ansGRTftrue 	:= GRTf(TRUE, 20.0101021899289, 10.999929877766277000287763902);
	
	
	
	
	
	
	
	//	LEQ compares the first UDINT (or smaller num) with the second returning postitve if the first is LESS OR EQUAL
	ansLEQtrue 		:= LEQ(TRUE, 10, 20);
	ansLEQfalse 	:= LEQ(TRUE, 20, 10);
	//	LEQf compares the first REAL (or smaller num) with the second returning postitve if the first is LESS OR EQUAL
	ansLEQftrue 	:= LEQf(TRUE, 10.166762776, 20.0000900009000917);
	ansLEQffalse 	:= LEQf(TRUE, 20.0101021899289, 10.999929877766277000287763902);
	
	
	
	
	
	
	//	LES compares the first UDINT (or smaller num) with the second returning postitve if the first is LESS 
	ansLEStrue 		:= LES(TRUE, 10, 20);
	ansLESfalse 		:= LES(TRUE, 20, 10);
	//	LESf compares the first REAL (or smaller num) with the second returning postitve if the first is LESS
	ansLESftrue		:= LESf(TRUE, 10.166762776, 20.0000900009000917);
	ansLESffalse 	:= LESf(TRUE, 20.0101021899289, 10.999929877766277000287763902);
	
	
	
	
	
	
	
	//	LIM compares the 3rd param (UDINT) with the 2nd and 4th returing true if it is less than the 2nd and higher than the 4th
	ansLIMtrue 		:= LIM(TRUE, 20, 15, 10);
	ansLIMfalse 	:= LIM(TRUE, 20, 25, 10);
	//	LIMf compares the 3rd param (REAL) with the 2nd and 4th returing true if it is less than the 2nd and higher than the 4th
	ansLIMftrue 	:= LIMf(TRUE, 20.0000900009000917, 15.333, 10.166762776);
	ansLIMffalse 	:= LIMf(TRUE, 20.0101021899289, 30.000288, 10.999929877766277000287763902);
	
	
	
	
	
	
	//	MEQ compares two values (DINT) with a mask (the middle DINT), this mask determines how many bits the two should be compared with
	//	This allows slighty different numbers to be said to be equal with the correct mask
	ansMEQtrue 		:= MEQ(TRUE, 20, 1, 22);
	ansMEQfalse 	:= MEQ(TRUE, 20, 2, 22); 		//Compares 1 more bit than the one above making it FALSE
	
	
	
	
	
	
	//	BOOLARR_TO_DINT Converts an array of BOOL[0..31] to a DINT 
	boolArray[1] 	:= TRUE;
	boolArray[2] 	:= TRUE;
	boolArray[3] 	:= TRUE;
	dintFROMbool 	:= BOOLARR_TO_DINT(ADR(boolArray));
	
	
	
	
	
	
	//DINT_TO_BOOLARR Converts a DINT to an array of BOOL[0..31], first param is the DINT, second is the ARRAY that is modified on the other end
	DINT_TO_BOOLARR(dintFORbool, ADR(boolArrayFROMdint));

	
	
	
	
	
	//	FLL fills the past array with the value of the fflVar, the array side takes in the byte size in the second paramter and the
	// 	amount of copying excecutions in the 3rd paramter, this amount correlates to the amount of array elements wished to be copied when used correctly
	//	(IF the 2nd element does NOT match the individual element size of the fflArray, there
	//	will be incorrect numbers copied)
	FLL(TRUE, SIZEOF(fflArray[0]), SIZEOF(fflArray) / SIZEOF(fflArray[0]), ADR(fflVar), ADR(fflArray));
	
	
	
	
	
	//	FFL takes in a number that is wished to be added to top of stack, the beginning of the stack, the length, and the number
	//	of the beginning (in this case 0)
	fflResult 		:= FFL(fflEnable, ADR(loadNum), ADR(FIFO[0]), ADR(fflControl), fifoLength, startingNum);
	//	FFU does the same except it takes the number out and loads it to unloadedNum
	ffuResult 		:= FFU(ffuEnable, ADR(FIFO[0]), ADR(unloadedNum), ADR(fflControl), fifoLength, startingNum);
	
	//	This ensures that it does not just added or remove the value to/from the entire array
	IF fflEnable THEN
		fflEnable := FALSE;
	END_IF
	
	IF ffuEnable THEN
		ffuEnable := FALSE;
	END_IF
	
	
	
	
	
	//	Copies binary numbers (Or digital but visually binary) with the second paramter controlling what is passes as each 
	//	1 in the binary number correlates to a translated number to the output from the mvmVar
	MVM(TRUE, maskNum, ADR(mvmVar), ADR(mvmOutput));
	
	
	
	
	
	
	//	ONS function block works like an EDGEPOS where placing the enable to true causes the output 'Out' to go 
	// 	TRUE for one scan
	
	ONS_0(Enable := onsEnable);
	onsBool 	:= ONS_0.Out;
	
	
	
	
	
	
	//	PULSE function takes in a TIMER_typ that has a PRE value set to the timing the output to should alternate
	// 	to TRUE at
	pulseOut 	:= PULSE(PulseEnable, ADR(pulseTimer));
		
	
	
	
	
	//	CTDf counts down to the PRE inside of the COUNTER_typ
	ctdOut 		:= CTDf(CtdfEnable, ADR(ctdfCounter));
	//	CTUf counts up to the PRE inside of the COUNTER_typ
	ctuOut 		:= CTUf(CtufEnable, ADR(ctufCounter));
	
	
	
	
	
	
	
	// 	REScounter rests the COUNTER_typ passed into it, although the type needs to be being used by something else for a difference
	// 	to be noticed
	resOut 		:= REScounter(RescEnable, ADR(resCounter));
	
	
	//	RTO enables a one time timer that keeps the output as TRUE when ACC >= PRE inside the TIMER_typ
	rtoOut 		:= RTO(RtoEnable, ADR(rtoTimer));
	
	
	//	RES sets the ACC of a TIMER_typ to 0 and keeps it at 0 until changed to false
	resOut 		:= RES(ResEnable, ADR(rtoTimer));

	
	//	TONf counts to the PRE value in the TIMER_typ then outputs a TRUE that stays
	tonfOut		:= TONf(TonfEnable, ADR(tonfTimer));
	
	
	
	
	
	
	
	//	STD calculates the standard deviation of an Array of numbers whose type 
	// 	needs to be sent in as the last parameter. 
	STD(stdEnable, ADR(numArray), ADR(stdANS), ADR(stdCONTROl), SIZEOF(numArray) / SIZEOF(numArray[0]), lhSTD_DATATYPE_INT);
	
	
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

