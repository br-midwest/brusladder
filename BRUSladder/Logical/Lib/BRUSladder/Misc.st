(********************************************************************
 * COPYRIGHT --  B&R Industrial Automation
 ********************************************************************
 * Library: LadderHelp
 * File: LadderOperator.st
 * Author: Brad Jones
 * Created: March 30, 2017
 ********************************************************************
 * LadderHelp Ladder Operators
 Each comp function has an d-int and a floating point version for type
 casting convenience and avoiding compiler warnings. Functions with 
 "f" suffix take REAL/float args

 These functions are designed to make working in ladder a bit more 
 hospitable
	-Boolean logic flows through these operations without use of EN/ENO
	-No FBs, so no declarations necessary
 ********************************************************************) 
 
(* toggle the output on and off with a rising edge input*)
FUNCTION_BLOCK Toggle

	IF Enable AND NOT(OldEnable) THEN
		Output	:= Output XOR 1;
	END_IF
	OldEnable	:= Enable; // Map last enable state
	
END_FUNCTION_BLOCK 



(*Convert an array of bool into a DINT*)
FUNCTION BOOLARR_TO_DINT
	
	array ACCESS pArr;
	
	BOOLARR_TO_DINT.0	:= array[0];
	BOOLARR_TO_DINT.1	:= array[1];
	BOOLARR_TO_DINT.2	:= array[2];
	BOOLARR_TO_DINT.3	:= array[3];
	BOOLARR_TO_DINT.4	:= array[4];
	BOOLARR_TO_DINT.5	:= array[5];
	BOOLARR_TO_DINT.6	:= array[6];
	BOOLARR_TO_DINT.7	:= array[7];
	BOOLARR_TO_DINT.8	:= array[8];
	BOOLARR_TO_DINT.9	:= array[9];
	BOOLARR_TO_DINT.10	:= array[10];
	BOOLARR_TO_DINT.11	:= array[11];
	BOOLARR_TO_DINT.12	:= array[12];
	BOOLARR_TO_DINT.13	:= array[13];
	BOOLARR_TO_DINT.14	:= array[14];
	BOOLARR_TO_DINT.15	:= array[15];
	BOOLARR_TO_DINT.16	:= array[16];
	BOOLARR_TO_DINT.17	:= array[17];
	BOOLARR_TO_DINT.18	:= array[18];
	BOOLARR_TO_DINT.19	:= array[19];
	BOOLARR_TO_DINT.20	:= array[20];
	BOOLARR_TO_DINT.21	:= array[21];
	BOOLARR_TO_DINT.22	:= array[22];
	BOOLARR_TO_DINT.23	:= array[23];
	BOOLARR_TO_DINT.24	:= array[24];
	BOOLARR_TO_DINT.25	:= array[25];
	BOOLARR_TO_DINT.26	:= array[26];
	BOOLARR_TO_DINT.27	:= array[27];
	BOOLARR_TO_DINT.28	:= array[28];
	BOOLARR_TO_DINT.29	:= array[29];
	BOOLARR_TO_DINT.30	:= array[30];
	BOOLARR_TO_DINT.31	:= array[31];
	
END_FUNCTION

(*Convert DINT to array of bool*)
FUNCTION DINT_TO_BOOLARR
	
	array ACCESS pArr;
	
	array[0] 	:= DintToConvert.0;
	array[1] 	:= DintToConvert.1;
	array[2] 	:= DintToConvert.2;
	array[3] 	:= DintToConvert.3;
	array[4] 	:= DintToConvert.4;
	array[5] 	:= DintToConvert.5;
	array[6] 	:= DintToConvert.6;
	array[7] 	:= DintToConvert.7;
	array[8]	:= DintToConvert.8;
	array[9] 	:= DintToConvert.9;
	array[10]	:= DintToConvert.10;
	array[11]	:= DintToConvert.11;
	array[12]	:= DintToConvert.12;
	array[13]	:= DintToConvert.13;
	array[14]	:= DintToConvert.14;
	array[15]	:= DintToConvert.15;
	array[16]	:= DintToConvert.16;
	array[17]	:= DintToConvert.17;
	array[18]	:= DintToConvert.18;
	array[19]	:= DintToConvert.19;
	array[20]	:= DintToConvert.20;
	array[21]	:= DintToConvert.21;
	array[22]	:= DintToConvert.22;
	array[23]	:= DintToConvert.23;
	array[24]	:= DintToConvert.24;
	array[25]	:= DintToConvert.25;
	array[26]	:= DintToConvert.26;
	array[27]	:= DintToConvert.27;
	array[28]	:= DintToConvert.28;
	array[29]	:= DintToConvert.29;
	array[30]	:= DintToConvert.30;
	array[31]	:= DintToConvert.31;
	
END_FUNCTION