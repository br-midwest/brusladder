(********************************************************************************
* COPYRIGHT --  B&R Industrial Automation
	*****************************************************************************
* Library: BRUSladder
* File: BTD.st
* Author: Marcus Mangel
* Created: January 21, 2020
	*****************************************************************************
	This function copies specified bits from a source variable to a
	destination variable. Any sequence of any size can be copied
	to any corresponding place in the destination. The bits in the destination
	which are not copied to are not changed.

	The copying starts at the source bit and copies the number of bits specified.
	If the number of bits overruns the size of the source, copying simply stops.
	The copied bits are then transferred to the destination, starting at the
	destination bit. If the copying would overrun the destination variable, copying 
	stops when the variable ends.

	This function will not perform any operations if
		-The source bit is larger than the size of the source variable
		-The destination bit is larger than the size of the destination
		-The number of bits to copy is set to zero

	The output to this function tells whether an operation was completed

	This function is meant to mimic the Allen-Bradley BTD instruction
*********************************************************************************)

FUNCTION BTD
		//Do not complete the operation if the bit position parameters are wrong, or if no bits are to be copied
	IF (SourceBit >= SourceSize*8 OR DestBit >= DestSize *8 OR NumBits = 0) THEN
		BTD := FALSE;
		RETURN;
	END_IF;
	
	//Copy Source and Destination from addresses into holding variables
	Source 	:= 0;
	Dest   	:= 0;
	
	brsmemcpy(ADR(Source),adrSrc,SourceSize);
	brsmemcpy(ADR(Dest),adrDest,DestSize);
	
	//Make sure the bounds of either variable are not overrun during copying
	//If the function is set to copy over memory, just cut off what cannot be copied and continue the operation
	MaxSourceBits := SourceSize*8 - SourceBit;
	MaxDestBits := DestSize*8 - DestBit;
	
	IF (NumBits > MaxSourceBits) THEN
		NumBits := MaxSourceBits;
	END_IF;
	
	IF (NumBits > MaxDestBits) THEN
		NumBits := MaxDestBits;
	END_IF;
	
	//Create a bitmask which determines which bits from the source should be copied
	BitmaskCounter := 0;
	Bitmask := 0;
		
	FOR BitmaskCounter := 0 TO 32 DO
		IF (BitmaskCounter >= SourceBit AND BitmaskCounter < (SourceBit + NumBits)) THEN
			Bitmask := BIT_SET(Bitmask,BitmaskCounter);
			//Bitmask = (1 << BitmaskCounter) - 1; This is the command in C
		END_IF;
	END_FOR;
	//Bitwise and the bitmask with the source to get the relevant bits
	SourceBitmask := Bitmask AND Source;
	
	//Create an inverted bitmask which determines the bits from the destination that should be changed
	BitmaskCounter := 0;
	InvertedBitmask := 0;
	FOR BitmaskCounter := 0 TO 32 DO
		IF (BitmaskCounter >= DestBit AND BitmaskCounter < (DestBit + NumBits)) THEN
			InvertedBitmask := BIT_SET(InvertedBitmask,BitmaskCounter);
			//InvertedBitmask := (1 << BitmaskCounter) - 1; This is the command in C
		END_IF;
	END_FOR;
	//Bitwise and the bitmask with the source to zero out the bits that need to be changed
	InvertedBitmask := NOT(InvertedBitmask);
	Dest := Dest AND InvertedBitmask;
	
	//Because bits from any place in the source can be copied to any place in the destination
	//shift the source bitmask left or right so that both sets of bits line up
	IF (SourceBit > DestBit) THEN
		SourceBitmask := SHR(SourceBitmask,(SourceBit - DestBit));
		//SourceBitmask := SourceBitmask >> (SourceBit - DestBit); This is the command in C
	ELSIF (SourceBit < DestBit) THEN
		SourceBitmask := SHL(SourceBitmask,(DestBit - SourceBit));
		//SourceBitmask := SourceBitmask << (DestBit - SourceBit); This is the command in C
	END_IF
	
	//Copy the source bits into the destination
	Dest := Dest OR SourceBitmask;
	
	//Copy the holding variable into the destination address
	brsmemcpy(adrDest,ADR(Dest),DestSize);
	
	BTD := TRUE;

END_FUNCTION
