(********************************************************************
 * COPYRIGHT --  B&R Industrial Automation
 ********************************************************************
 * Library: LadderHelp
 * File: LadderOperator.st
 * Author: Brad Jones
 * Created: March 30, 2017
 ********************************************************************
 * LadderHelp Ladder Operators
 Each comp function has an d-int and a floating point version for type
 casting convenience and avoiding compiler warnings. Functions with 
 "f" suffix take REAL/float args

 These functions are designed to make working in ladder a bit more 
 hospitable
	-Boolean logic flows through these operations without use of EN/ENO
	-No FBs, so no declarations necessary
 ********************************************************************) 
 
(*Calculates the average of an array*)
FUNCTION AVE
	
	IF Enable THEN
		// set addresses
		result ACCESS pDest;
		Control ACCESS pControl;	
		
		Control.EN	:= Enable;
		Control.LEN	:= Length;
		
		// sum the elements
		FOR i := 0 TO Length - 1 DO
			CASE Datatype OF
				brusDATATYPE_SINT:		
					brsmemmove(ADR(oneOfEach.mySINT), pArray + (i*1), 1);
					sum	:= sum + SINT_TO_LREAL(oneOfEach.mySINT);
						
				brusDATATYPE_USINT:		
					brsmemmove(ADR(oneOfEach.myUSINT), pArray + (i*1), 1);
					sum	:= sum + USINT_TO_LREAL(oneOfEach.myUSINT);
						
				brusDATATYPE_INT:		
					brsmemmove(ADR(oneOfEach.myINT), pArray + (i*2), 2);
					sum	:= sum + INT_TO_LREAL(oneOfEach.myINT);
						
				brusDATATYPE_UINT:		
					brsmemmove(ADR(oneOfEach.myUINT), pArray + (i*2), 2);
					sum	:= sum + UINT_TO_LREAL(oneOfEach.myUINT);
						
				brusDATATYPE_DINT:		
					brsmemmove(ADR(oneOfEach.myDINT), pArray + (i*4), 4);
					sum	:= sum + DINT_TO_LREAL(oneOfEach.myDINT);
						
				brusDATATYPE_UDINT:		
					brsmemmove(ADR(oneOfEach.myUDINT), pArray + (i*4), 4);
					sum	:= sum + UDINT_TO_LREAL(oneOfEach.myUDINT);
						
				brusDATATYPE_REAL:		
					brsmemmove(ADR(oneOfEach.myREAL), pArray + (i*4), 4);
					sum	:= sum + REAL_TO_LREAL(oneOfEach.myREAL);
						
				brusDATATYPE_LREAL:		
					brsmemmove(ADR(oneOfEach.myLREAL), pArray + (i*8), 8);
					sum	:= sum + (oneOfEach.myLREAL);
				ELSE
					Control.ER	:= TRUE;
			END_CASE;
			Control.POS	:= i;
		END_FOR
				
		// return average
		result		:= LREAL_TO_REAL(sum / Length);
		Control.DN	:= TRUE;
		Control.ER	:= FALSE;	
	END_IF
	AVE	:= Enable;
END_FUNCTION
	


(* Converts BIT operations from ST to LD *)
FUNCTION BITTST
	BITTST	:= Enable AND (BIT_TST(Source, Bit) XOR Polarity);
END_FUNCTION
FUNCTION BITCLR
	Source ACCESS pSrc;
	IF Enable THEN
		Source	:= BIT_CLR(Source, Bit);
	END_IF
	BITCLR	:= Enable;
END_FUNCTION
FUNCTION BITSET
	Source ACCESS pSrc;
	IF Enable THEN
		Source	:= BIT_SET(Source, Bit);
	END_IF
	BITSET	:= Enable;
END_FUNCTION



(*Compares whether  Var1 is equal to Var2*)
FUNCTION EQU
	EQU:= Enable AND (Var1 = Var2);
END_FUNCTION
(*Floating point version*)
FUNCTION EQUf
	EQUf:= Enable AND (Var1 = Var2);
END_FUNCTION
	


(*Loads value into 4 byte FIFO*)
FUNCTION FFL
	Control ACCESS pControl;
	
	IF Enable THEN		
		Control.EN	:= Enable;
		IF Control.LEN = 0 OR Length > 0 THEN
			Control.LEN	:= Length;
		END_IF
		
		// accomodate new values
		IF Control.POS < Control.LEN THEN
			FOR i := Control.POS - 1 TO 0 BY -1 DO
				brsmemcpy(pFIFO + (4 * (i + 1)), pFIFO + (4 * i), 4);
			END_FOR	
			
			Control.POS	:= Control.POS + 1;// increment POS to show new value
			brsmemcpy(pFIFO, pSrc, 4);// add new value to FIFO
			Control.DN	:= FALSE; // load is done when FIFO is full
		END_IF
		
		Control.DN	:= (Control.POS = Control.LEN);
		Control.EM	:= (Control.POS = 0);
	END_IF
	FFL	:= Enable;
END_FUNCTION
	


(*Unloads value from dint FIFO, returns 0 if empty*)
FUNCTION FFU
	Control ACCESS pControl;
	
	IF Enable THEN
		Control.EU	:= Enable;
		// only take the Length input on the function if necessary
		IF Control.LEN = 0 OR Length > 0 THEN
			Control.LEN	:= Length;
		END_IF
	
		IF Control.POS > 0 THEN
			// Pull value from top of FIFO
			brsmemcpy(pDest, pFIFO + (4 * (Control.POS - 1)), 4);
			
			// clear that value
			brsmemset(pFIFO + (4 * (Control.POS - 1)), 0, 4);
			
			// decrement POS to show new value
			Control.POS	:= Control.POS - 1;
			Control.DN	:= FALSE;
		ELSIF Control.POS = 0 THEN
			brsmemset(pDest, 0, 4); // if an unload is called with empty FIFO return 0
		END_IF
		
		Control.DN	:= // unload is DN when FIFO is empty
		Control.EM	:= (Control.POS = 0);
	END_IF
	FFU	:= Enable;
END_FUNCTION



(*Copies source into destination*)
FUNCTION FLL
	IF Enable AND Length > 0 THEN
		FOR i := 0 TO Length-1 DO
			brsmemmove(pDest + (i*SizeOfElement), pSource, SizeOfElement);
		END_FOR
	END_IF
	FLL	:= Enable;
END_FUNCTION


(* compares values in an array, element by element, whether or not they are less than 80 *)
FUNCTION FSC
	
	IF Enable THEN
		Control ACCESS pControl;
		IF Control.LEN = 0 THEN
			Control.LEN	:= Length;
		END_IF
		
		IF NOT Control.IN THEN
			// in AB code it always sets
			FOR Control.POS := 1 TO Control.LEN - 1 DO
				testValue ACCESS pArray + (Control.POS);
				IF testValue < 80 THEN
					Control.IN	:= TRUE;
					Control.FD	:= TRUE;
					EXIT;
				END_IF
			END_FOR
		END_IF
	END_IF
	Control.DN	:= Control.POS >= Length - 1;
	Control.EN	:=
	FSC			:= Enable;
END_FUNCTION



(* Checks if Var1 is greater than Var2*)
FUNCTION GEQ
	GEQ:= Enable AND (Var1 >= Var2);
END_FUNCTION
(*Floating point version*)
FUNCTION GEQf
	GEQf:= Enable AND (Var1 >= Var2);
END_FUNCTION



(*Compares whether Var1 is greater than Var2
 * Example evaluation:

	Var1 = 5.9
	Var2 = 5.8
	Result = TRUE 							*)
FUNCTION GRT
	GRT:= Enable AND (Var1 > Var2);
END_FUNCTION
(*Floating point version*)
FUNCTION GRTf
	GRTf:= Enable AND (Var1 > Var2);
END_FUNCTION



(*Compares whether Var1 is less than Var2*)
FUNCTION LEQ
	LEQ:= Enable AND (Var1 <= Var2);
END_FUNCTION
(*Floating point version*)
FUNCTION LEQf
	LEQf:= Enable AND (Var1 <= Var2);
END_FUNCTION



(*Checks if  Var1 is less than Var2*)
FUNCTION LES
	LES:= Enable AND (Var1 < Var2);
END_FUNCTION
(*Floating point version*)
FUNCTION LESf
	LESf:= Enable AND (Var1 < Var2);
END_FUNCTION



(* Checks to see if two Intergers are within the high and low limit*)
FUNCTION LIM
	LIM:= Enable AND (TestVar <= HighLimit) AND (TestVar >= LowLimit);
END_FUNCTION
(*Floating point version*)
FUNCTION LIMf
	LIMf:= Enable AND (TestVar <= HighLimit) AND (TestVar >= LowLimit);
END_FUNCTION



(* masked equivalency *)
FUNCTION MEQ
	MEQ := Enable AND ((Source AND Mask) = (Compare AND Mask));
END_FUNCTION



(* Does a masked move of source into dest *)
FUNCTION MVM
	myDest ACCESS pDest;
	mySource ACCESS pSource;
	IF Enable THEN
		FOR mvmi := 0 TO 31 DO
			IF BIT_TST(Mask, mvmi) THEN
				IF BIT_TST(mySource, mvmi) THEN
					myDest	:= BIT_SET(myDest, mvmi);
				ELSE
					myDest	:= BIT_CLR(myDest, mvmi);
				END_IF
			END_IF
		END_FOR
	END_IF
	MVM	:= Enable;
END_FUNCTION



(*Compares whether Var1 is not equal to Var2*)
FUNCTION NEQ
	NEQ	:= Enable AND (Var1 <> Var2);
END_FUNCTION
(*Floating point version*)
FUNCTION NEQf
	NEQf	:= Enable AND (Var1 <> Var2);
END_FUNCTION



FUNCTION_BLOCK ONS
	Out := Enable AND NOT(storageBit);
	storageBit	:= Enable;	 
END_FUNCTION_BLOCK



(*Calculates the standard deviation of an array of DINT values*)
FUNCTION STD

	IF Enable THEN
		Control ACCESS pControl;
		result ACCESS pDest;
		
		Control.EN	:= Enable;
		Control.LEN	:= Length;
		
		AVE(1, pArray, ADR(stdAvg), ADR(aveCtrl), Length, Datatype); 
		
		FOR i := 0 TO Length - 1 DO
			CASE Datatype OF
				brusDATATYPE_SINT:		
					brsmemmove(ADR(oneOfEach.mySINT), pArray + (i*1), 1);
					sum	:= sum + ((SINT_TO_LREAL(oneOfEach.mySINT) - stdAvg)**2);
						
				brusDATATYPE_USINT:		
					brsmemmove(ADR(oneOfEach.myUSINT), pArray + (i*1), 1);
					sum	:= sum + ((USINT_TO_LREAL(oneOfEach.myUSINT) - stdAvg)**2);
						
				brusDATATYPE_INT:		
					brsmemmove(ADR(oneOfEach.myINT), pArray + (i*2), 2);
					sum	:= sum + ((INT_TO_LREAL(oneOfEach.myINT) - stdAvg)**2);
						
				brusDATATYPE_UINT:		
					brsmemmove(ADR(oneOfEach.myUINT), pArray + (i*2), 2);
					sum	:= sum + ((UINT_TO_LREAL(oneOfEach.myUINT) - stdAvg)**2);
						
				brusDATATYPE_DINT:		
					brsmemmove(ADR(oneOfEach.myDINT), pArray + (i*4), 4);
					sum	:= sum + ((DINT_TO_LREAL(oneOfEach.myDINT) - stdAvg)**2);
						
				brusDATATYPE_UDINT:		
					brsmemmove(ADR(oneOfEach.myUDINT), pArray + (i*4), 4);
					sum	:= sum + ((UDINT_TO_LREAL(oneOfEach.myUDINT) - stdAvg)**2);
						
				brusDATATYPE_REAL:		
					brsmemmove(ADR(oneOfEach.myREAL), pArray + (i*4), 4);
					sum	:= sum + ((REAL_TO_LREAL(oneOfEach.myREAL) - stdAvg)**2);
						
				brusDATATYPE_LREAL:		
					brsmemmove(ADR(oneOfEach.myLREAL), pArray + (i*8), 8);
					sum	:= sum + (((oneOfEach.myLREAL) - stdAvg)**2);
				ELSE
					Control.ER	:= TRUE;
			END_CASE;
			Control.POS	:= i;
		END_FOR
		
		result := SQRT((1.0 / Length) * sum);
		
		Control.DN	:= TRUE;
		Control.ER	:= FALSE;
	END_IF
	STD	:= Enable;	
END_FUNCTION



(*Rearranges bytes in a 4 byte value*)
FUNCTION SWPB
	IF Enable THEN
		CASE OrderMode OF
			lhSWPB_MODE_WORD: // words are swapped
				brsmemmove(ADR(tempBytes), pSource+2, 2);
				brsmemmove(ADR(tempBytes)+2, pSource, 2); 
			
			lhSWPB_MODE_HIGHLOW: // high and low bytes of words swapped
				brsmemmove(ADR(tempBytes), pSource+1, 1);
				brsmemmove(ADR(tempBytes)+1, pSource, 1);
				
				brsmemmove(ADR(tempBytes)+2, pSource+3, 1);
				brsmemmove(ADR(tempBytes)+3, pSource+2, 1);
			
			lhSWPB_MODE_REVERSE: // byte order is reversed
				FOR i := 0 TO 3 DO
					brsmemmove(ADR(tempBytes)+3-i, pSource+i, 1);
				END_FOR
		END_CASE;
		// export the swapped bytes
		brsmemmove(pDest ,ADR(tempBytes), 4);
	END_IF
	SWPB	:= Enable;
END_FUNCTION