(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: LimTest
 * File: LadderHelp.typ
 * Author: Brad Jones
 * Created: July 20, 2017
 ********************************************************************
 * Data types of library LadderHelp
 ********************************************************************)

TYPE
	debug_typ : {REDUND_UNREPLICABLE} 	STRUCT 
		dint : {REDUND_UNREPLICABLE} DINT;
		lreal : {REDUND_UNREPLICABLE} LREAL;
		real : {REDUND_UNREPLICABLE} REAL;
	END_STRUCT;
	OneOfEach_typ : 	STRUCT 
		mySINT : SINT; (*DO NOT USE, INTERNAL STRUCTURE*)
		myUSINT : USINT; (*DO NOT USE, INTERNAL STRUCTURE*)
		myINT : INT; (*DO NOT USE, INTERNAL STRUCTURE*)
		myUINT : UINT; (*DO NOT USE, INTERNAL STRUCTURE*)
		myDINT : DINT; (*DO NOT USE, INTERNAL STRUCTURE*)
		myUDINT : UDINT; (*DO NOT USE, INTERNAL STRUCTURE*)
		myREAL : REAL; (*DO NOT USE, INTERNAL STRUCTURE*)
		myLREAL : LREAL; (*DO NOT USE, INTERNAL STRUCTURE*)
	END_STRUCT;
	CONTROL_typ : 	STRUCT 
		EN : BOOL; (*Indicates FFL is enabled*)
		EU : BOOL; (*Indicates FFU is enabled*)
		DN : BOOL; (*For FFL, indicates that the FIFO is full. For FSC, indicates POS=LEN (search is done)*)
		EM : BOOL; (*Indicates that the FIFO is empty*)
		ER : BOOL; (*Indicates an error on the instruction*)
		UL : BOOL; (*NOT YET USED*)
		IN : BOOL; (*Inhibit bit, indicates that FSC detected a true comparison. This must be cleared in order to continue searching.*)
		FD : BOOL; (*Found bit, indicates the FSC instruction detected a true comparison*)
		LEN : DINT; (*FFL,FFU: size of the FIFO. FSC: number of elements to search from src array *)
		POS : DINT; (*FFL,FFU: Used as the starting point in the FIFO array. FSC: current position being accessed*)
	END_STRUCT;
	TIMER_typ : 	STRUCT 
		PRE : DINT; (*The preset value specifies the value (1 msec units) which the accumulated value must reach before the instruction sets the .DN bit*)
		ACC : DINT; (*The accumulated value specifies the number of milliseconds that have elapsed since the TON instruction was enabled*)
		EN : BOOL; (*The enable bit indicates the TON instruction is enabled*)
		TT : BOOL; (*The timing bit indicates that a timing operation is in process*)
		DN : BOOL; (*The done bit indicates that ACC >= PRE*)
		IS : zTIMER_IS_typ; (*DO NOT USE, INTERNAL STRUCTURE*)
	END_STRUCT;
	COUNTER_typ : 	STRUCT 
		PRE : DINT; (*The preset value specifies the value which the accumulated value must reach before the instruction sets the .DN bit.*)
		ACC : DINT; (*The accumulated value specifies the number of transitions the instruction has counted.*)
		CD : BOOL; (*The count down enable bit indicates the CTD instruction is enabled.*)
		CU : BOOL; (*The count up enable bit indicates the CTU instruction is enabled.*)
		UN : BOOL; (*The underflow bit indicates that the counter exceeded the lower limit of -2,147,483,647. The counter then rolls over to 2,147,483,647 and begins counting down again.*)
		OV : BOOL; (*The overflow bit indicates the counter exceeded the upper limit of 2,147,483,647. The counter then rolls over to -2,147,483,648 and begins counting up again.*)
		DN : BOOL; (*The done bit indicates that .ACC >= .PRE.*)
		IS : zCOUNTER_IS_typ; (*DO NOT USE, INTERNAL STRUCTURE*)
	END_STRUCT;
	zCOUNTER_IS_typ : 	STRUCT 
		prevCounter : DINT; (*DO NOT USE, INTERNAL STRUCTURE*)
		old_CD : BOOL; (*DO NOT USE, INTERNAL STRUCTURE*)
		old_CU : BOOL; (*DO NOT USE, INTERNAL STRUCTURE*)
	END_STRUCT;
	zTIMER_IS_typ : 	STRUCT 
		startTime : TIME; (*DO NOT USE, INTERNAL STRUCTURE*)
		currentTime : TIME; (*DO NOT USE, INTERNAL STRUCTURE*)
		lastScanTime : TIME; (*DO NOT USE, INTERNAL STRUCTURE*)
		oldEnable : BOOL; (*DO NOT USE, INTERNAL STRUCTURE*)
		reset : BOOL; (*DO NOT USE, INTERNAL STRUCTURE*)
	END_STRUCT;
END_TYPE
