(********************************************************************
 * COPYRIGHT -- B&R Industrial Automation
 ********************************************************************
 * Library: LadderHelp
 * File: LadderHelp.fun
 * Author: Brad Jones
 * Created: March 21, 2017
 ********************************************************************
 * Functions and function blocks to assist in the development of LD 
 * programs. Functions with the *f suffix are designed for floating
 * point operations
 ********************************************************************)

FUNCTION AVE : BOOL (*Returns the average of an array of values*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pArray : UDINT; (*Address of where to begin averaging*) (* *) (*#PAR*)
		pDest : UDINT; (*Pointer to REAL result (average of pArray)*) (* *) (*#PAR*)
		pControl : UDINT; (*Address of the control structure*) (* *) (*#PAR*)
		Length : UDINT; (*Number of elements to average*) (* *) (*#PAR*)
		Datatype : USINT; (*Datatype of array (use lhAVE_ prefix)*) (* *) (*#PAR*)
	END_VAR
	VAR
		i : UDINT; (* *) (* *) (*#OMIT*)
		sum : LREAL; (* *) (* *) (*#OMIT*)
		oneOfEach : OneOfEach_typ; (* *) (* *) (*#OMIT*)
		Control : REFERENCE TO CONTROL_typ; (* *) (* *) (*#OMIT*)
		result : REFERENCE TO REAL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION BITCLR : BOOL (*ST keyword BIT_CLR in LD*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pSrc : UDINT; (*Pointer to value to modify*) (* *) (*#PAR*)
		Bit : DINT; (*Which bit in Src to clear*) (* *) (*#PAR*)
	END_VAR
	VAR
		Source : REFERENCE TO DINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION BITSET : BOOL (*ST keyword BIT_SET in LD*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pSrc : UDINT; (*Pointer to value to modify*) (* *) (*#PAR*)
		Bit : DINT; (*Which bit in Src to set*) (* *) (*#PAR*)
	END_VAR
	VAR
		Source : REFERENCE TO DINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION BITTST : BOOL (*ST keyword BIT_TST in LD*)
	VAR_INPUT
		Enable : BOOL; (*Enables the function*) (* *) (*#PAR*)
		Source : DINT; (*Value to test*) (* *) (*#PAR*)
		Bit : DINT; (*Which bit of Source to examine*) (* *) (*#PAR*)
		Polarity : BOOL; (*Determines whether scan is XIO or XIC. Use prefix lhBITTST_*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION BTD : BOOL (*Copies specified bits from the source to the destination*)
	VAR_INPUT
		adrSrc : UDINT; (*Address of the source variable*)
		SourceSize : UDINT; (*Size of the source variable, in bytes*)
		SourceBit : UINT; (*Bit at which to start copying from source*)
		adrDest : UDINT; (*Address of the destination variable*)
		DestSize : UDINT; (*Size of the destination variable, in bytes*)
		DestBit : UINT; (*Bit at which to start copying to destination*)
		NumBits : UDINT; (*Number of bits to copy*)
	END_VAR
	VAR
		Source : DINT;
		Dest : DINT;
		MaxSourceBits : UDINT;
		MaxDestBits : UDINT;
		BitmaskCounter : USINT;
		Bitmask : UDINT;
		InvertedBitmask : UDINT;
		SourceBitmask : UDINT;
	END_VAR
END_FUNCTION

FUNCTION BOOLARR_TO_DINT : DINT (*Converts an array of BOOL[0..31] to a DINT*)
	VAR_INPUT
		pArr : UDINT; (*Pointer to array BOOL[0..31]*) (* *) (*#PAR*)
	END_VAR
	VAR
		array : REFERENCE TO ARRAY[0..31] OF BOOL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION CTDf : BOOL (*Logix-style count down function. Returns DN bit*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pCounter : UDINT; (*Pointer to COUNTER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO COUNTER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION CTUf : BOOL (*Logix-style count up function. Returns DN bit*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pCounter : UDINT; (*Pointer to COUNTER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO COUNTER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION DINT_TO_BOOLARR : BOOL (*Converts a DINT to an array of BOOL[0..31] *)
	VAR_INPUT
		DintToConvert : DINT; (*Source DINT*)
		pArr : UDINT; (*Pointer to array BOOL[0..31]*) (* *) (*#PAR*)
	END_VAR
	VAR
		array : REFERENCE TO ARRAY[0..31] OF BOOL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION EQU : BOOL (*Compares whether Var1 is equal to Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : DINT; (*First argument to check*) (* *) (*#PAR*)
		Var2 : DINT; (*Second argument to check*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION EQUf : BOOL (*Compares whether floating point Var1 is equal to Var2*)
	VAR_INPUT
		Enable : BOOL; (* *) (* *) (*#PAR*)
		Var1 : REAL; (* *) (* *) (*#PAR*)
		Var2 : REAL; (* *) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION FFL : BOOL (*Loads value into a FIFO*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pSrc : UDINT; (*Pointer to value to load into FIFO*) (* *) (*#PAR*)
		pFIFO : UDINT; (*Pointer to the start of the FIFO*) (* *) (*#PAR*)
		pControl : UDINT; (*Pointer to CONTROL_typ (typically same as FFU)*) (* *) (*#PAR*)
		Length : DINT; (*If >0, used as length of FIFO, otherwise CONTROL_typ.LEN is used*) (* *) (*#PAR #OPT*)
		Position : DINT; (*If >0, used as the starting position within FIFO array, otherwise CONTROL_typ.POS is used*) (* *) (*#PAR #OPT*)
	END_VAR
	VAR
		Control : REFERENCE TO CONTROL_typ; (* *) (* *) (*#OMIT*)
		i : DINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION FFU : BOOL (*Unloads value from a FIFO*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pFIFO : UDINT; (*Pointer to the start of the FIFO*) (* *) (*#PAR*)
		pDest : UDINT; (*Pointer to where to unload values from the FIFO*) (* *) (*#PAR*)
		pControl : UDINT; (*Pointer to CONTROL_typ (typically same as FFL)*) (* *) (*#PAR*)
		Length : DINT; (*If >0, used as length of FIFO, otherwise CONTROL_typ.LEN is used*) (* *) (*#PAR #OPT*)
		Position : DINT; (*If >0, used as the starting position within FIFO array, otherwise CONTROL_typ.POS is used*) (* *) (*#PAR #OPT*)
	END_VAR
	VAR
		Control : REFERENCE TO CONTROL_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION FLL : BOOL (*Copies data into elements of an array, like memset for values >1 byte*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		SizeOfElement : DINT; (*Size of each element to be filled*) (* *) (*#PAR*)
		Length : DINT; (*Number of source elements to fill*) (* *) (*#PAR*)
		pSource : UDINT; (*Pointer to value with which the array will be filled*) (* *) (*#PAR*)
		pDest : UDINT; (*Pointer to array to be filled*) (* *) (*#PAR*)
	END_VAR
	VAR
		i : DINT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION FSC : BOOL (*ONLY WORKS WITH SINTS RN, USE NOT RECOMMENDED*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pControl : UDINT; (*Address of the control structure*) (* *) (*#PAR*)
		pArray : UDINT; (*Array to be searched*) (* *) (*#PAR*)
		Length : DINT; (*Number of elements to search*) (* *) (*#PAR*)
		Position : DINT; (*Offset into the array*) (* *) (*#PAR*)
		Mode : UDINT; (*not used*) (* *) (*#PAR #OPT*)
		Expression : UDINT; (*not used*) (* *) (*#PAR #OPT*)
	END_VAR
	VAR
		testValue : REFERENCE TO SINT; (* *) (* *) (*#OMIT*)
		Control : REFERENCE TO CONTROL_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION GEQ : BOOL (*Compares whether Var1 is greater than or equal Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : DINT; (*Input 1, DINT*) (* *) (*#PAR*)
		Var2 : DINT; (*Input 2, DINT*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION GEQf : BOOL (*Compares whether floating point Var1 is greater than or equal Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : REAL; (*Input 1, REAL*) (* *) (*#PAR*)
		Var2 : REAL; (*Input 2, REAL*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION GRT : BOOL (*Compares whether Var1 is greater than Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : DINT; (*Input 1, DINT*) (* *) (*#PAR*)
		Var2 : DINT; (*Input 2, DINT*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION GRTf : BOOL (*Compares whether floating point Var1 is greater than Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : REAL; (*Input 1, REAL*) (* *) (*#PAR*)
		Var2 : REAL; (*Input 2, REAL*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION LEQ : BOOL (*Compares whether Var1 is less than or equal Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : DINT; (*Input 1, DINT*) (* *) (*#PAR*)
		Var2 : DINT; (*Input 2, DINT*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION LEQf : BOOL (*Compares whether floating point Var1 is less than or equal Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : REAL; (*Input 1, REAL*) (* *) (*#PAR*)
		Var2 : REAL; (*Input 2, REAL*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION LES : BOOL (*Compares whether Var1 is less than Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : DINT; (*Input 1, DINT*) (* *) (*#PAR*)
		Var2 : DINT; (*Input 2, DINT*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION LESf : BOOL (*Compares whether floating point Var1 is less than Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : REAL; (*Input 1, REAL*) (* *) (*#PAR*)
		Var2 : REAL; (*Input 2, REAL*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION LIM : BOOL (*Compares whether test value is between limits (rollover mode when low value > high value)*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		HighLimit : DINT; (*Value of upper limit*) (* *) (*#PAR*)
		TestVar : DINT; (*Value to test against limits*) (* *) (*#PAR*)
		LowLimit : DINT; (*Value of lower limit*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION LIMf : BOOL (*Compares whether floating point test value is between limits (rollover mode when low value > high value)*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		HighLimit : REAL; (*Value of upper limit*) (* *) (*#PAR*)
		TestVar : REAL; (*Value to test against limits*) (* *) (*#PAR*)
		LowLimit : REAL; (*Value of lower limit*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION MEQ : BOOL (*Compares whether two values are equal through a mask*)
	VAR_INPUT
		Enable : BOOL; (*Enables function*) (* *) (*#PAR*)
		Source : DINT; (*Value to test against compare*) (* *) (*#PAR*)
		Mask : DINT; (*Defines which bits to block/mask*) (* *) (*#PAR*)
		Compare : DINT; (*Compare value*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION MVM : BOOL (*Copies the Source to a Destination and allows portions of the data to be masked.*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Mask : DINT; (*Defines which bits to block or pass*) (* *) (*#PAR*)
		pSource : UDINT; (*Pointer to value to move*) (* *) (*#PAR*)
		pDest : UDINT; (*Pointer to where to store result*) (* *) (*#PAR*)
	END_VAR
	VAR
		mySource : REFERENCE TO DINT; (* *) (* *) (*#OMIT*)
		myDest : REFERENCE TO DINT; (* *) (* *) (*#OMIT*)
		mvmi : INT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION NEQ : BOOL (*Compares whether Var1 is not equal to Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : DINT; (*Input 1, DINT*) (* *) (*#PAR*)
		Var2 : DINT; (*Input 2, DINT*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION NEQf : BOOL (*Compares whether floating point Var1 is not equal to Var2*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		Var1 : REAL; (*Input 1, REAL*) (* *) (*#PAR*)
		Var2 : REAL; (*Input 2, REAL*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION PULSE : BOOL (*Logix-style time-on delay. Returns DN bit*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pTimer : UDINT; (*Pointer to TIMER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO TIMER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION RES : BOOL (*Resets a timer*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pTimer : UDINT; (*Pointer to TIMER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO TIMER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION REScounter : BOOL (*Resets a timer*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pCounter : UDINT; (*Pointer to COUNTER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO COUNTER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION RTO : BOOL (*Logix-style retentive time-on delay. Returns DN bit*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pTimer : UDINT; (*Pointer to TIMER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO TIMER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION STD : BOOL (*Calculates standard deviation of an array of DINTS*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pArray : UDINT; (*Address of where to begin analysis*) (* *) (*#PAR*)
		pDest : UDINT; (*Address of where to put the standard deviation in floating point format*) (* *) (*#PAR*)
		pControl : UDINT; (*Address of the control structure*) (* *) (*#PAR*)
		Length : UDINT; (*Number of elements to average*) (* *) (*#PAR*)
		Datatype : USINT; (*Datatype of array (use lhSTD_ prefix)*) (* *) (*#PAR*)
	END_VAR
	VAR
		i : UDINT; (* *) (* *) (*#OMIT*)
		stdAvg : DINT; (* *) (* *) (*#OMIT*)
		oneOfEach : OneOfEach_typ; (* *) (* *) (*#OMIT*)
		sum : LREAL; (* *) (* *) (*#OMIT*)
		aveCtrl : CONTROL_typ; (* *) (* *) (*#OMIT*)
		result : REFERENCE TO REAL; (* *) (* *) (*#OMIT*)
		Control : REFERENCE TO CONTROL_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION SWPB : BOOL (*Rearranges the bytes in a 4 byte value*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pSource : UDINT; (*Address of source bytes*) (* *) (*#PAR*)
		OrderMode : UDINT; (*How to move the bytes (use lhSWPB_MODE_ prefix)*) (* *) (*#PAR*)
		pDest : UDINT; (*Address of swapped bytes*) (* *) (*#PAR*)
	END_VAR
	VAR
		tempBytes : UDINT; (* *) (* *) (*#OMIT*)
		i : INT; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK Toggle (*Toggles Output*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Output : BOOL; (*On +edge of Enable, Out is toggled*) (* *) (*#PAR*)
	END_VAR
	VAR
		OldEnable : BOOL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TON_ab
	VAR_INPUT
		EN : BOOL;
		PRE : DINT;
	END_VAR
	VAR_OUTPUT
		TT : BOOL;
		DN : BOOL;
		ACC : DINT;
	END_VAR
	VAR
		TON_FB : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TOF_ab
	VAR_INPUT
		EN : BOOL;
		PRE : DINT;
	END_VAR
	VAR_OUTPUT
		TT : BOOL;
		DN : BOOL;
		ACC : DINT;
	END_VAR
	VAR
		TOF_FB : TOF;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION TONf : BOOL (*Logix-style time-on delay. Returns DN bit*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
		pTimer : UDINT; (*Pointer to TIMER_typ*) (* *) (*#PAR*)
	END_VAR
	VAR
		this : REFERENCE TO TIMER_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK ONS (*One-Shot style instruction. Works like EDGEPOS or rTrig*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the instruction*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Out : BOOL; (*When Enable goes from false to true, Out is true for 1 scan.*) (* *) (*#PAR*)
	END_VAR
	VAR
		storageBit : BOOL; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK
