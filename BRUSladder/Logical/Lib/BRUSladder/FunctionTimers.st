(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Library: LadderHelp
 * File: FunctionTimers.st
 * Author:    Brad Jones
 * Created:   March 21, 2017/09:25 
*********************************************************
[PROGRAM INFORMATION]
Implements timers designed with ladder in mind
All timers in this library are functions based on a TIMER
variable. Passing the ADR of a PV of TIMER_typ allows each
function to perform its action on a given timer PV.
********************************************************)


(* counts upwards *)
FUNCTION CTUf
	this ACCESS pCounter;
	this.CU	:= Enable;
	
	this.IS.prevCounter	:= this.ACC;
	IF this.CU AND NOT this.IS.old_CU THEN
		this.ACC	:= this.ACC + 1;
	END_IF
	
	// set output
	this.DN	:= CTUf	:= this.ACC >= this.PRE;
	
	// detect over runs
	IF this.ACC = 2#1000_0000_0000_0000_0000_0000_0000_0000 AND this.IS.prevCounter = 2#0111_1111_1111_1111_1111_1111_1111_1111 THEN
		this.OV	:= TRUE;
	END_IF
	
	this.IS.old_CU	:= this.CU;
END_FUNCTION



(* counts upwards *)
FUNCTION CTDf
	this ACCESS pCounter;
	this.CD	:= Enable;
	
	this.IS.prevCounter	:= this.ACC;
	IF this.CD AND NOT this.IS.old_CD THEN
		this.ACC	:= this.ACC - 1;
	END_IF
	
	// set output
	this.DN	:= CTDf	:= this.ACC >= this.PRE;
	
	// detect under runs
	IF this.IS.prevCounter = 2#1000_0000_0000_0000_0000_0000_0000_0000 AND this.ACC = 2#0111_1111_1111_1111_1111_1111_1111_1111 THEN
		this.UN	:= TRUE;
	END_IF
	
	this.IS.old_CD	:= this.CD;
END_FUNCTION



(* Time on function *)
FUNCTION TONf
	
	this ACCESS pTimer;
	this.EN	:= enable; // use function input as this.EN to keep rung logic flow

	IF this.EN THEN
		IF NOT(this.IS.oldEnable) OR this.IS.reset THEN
			this.IS.reset		:= FALSE;
			this.IS.startTime	:= clock_ms();
		END_IF
		
		// set enabled outputs
		this.IS.currentTime	:= clock_ms();
		this.ACC			:= MIN(TIME_TO_DINT(this.IS.currentTime - this.IS.startTime), this.PRE);
		this.DN				:= this.ACC >= this.PRE;
	ELSE // function is disabled, act as reset
		this.ACC	:= 0;
		this.DN		:= FALSE;
		this.TT		:= FALSE;
	END_IF
		
	// set outputs
	this.TT				:= this.EN AND NOT(this.DN);
	TONf				:= this.DN;
	this.IS.oldEnable	:= this.EN;
		
END_FUNCTION


(* Retentive time on function *)
FUNCTION RTO
	
	this ACCESS pTimer;
	this.EN	:= enable; // use function input as this.EN to keep rung logic flow

	IF this.EN THEN
		IF NOT(this.IS.oldEnable) OR this.IS.reset THEN
			this.IS.reset		:= FALSE;
			this.IS.lastScanTime:= clock_ms();
		ELSE
			this.IS.lastScanTime:= this.IS.currentTime;
		END_IF
		
		// set enabled outputs
		this.IS.currentTime	:= clock_ms();
		this.ACC			:= MIN(this.ACC + TIME_TO_DINT(this.IS.currentTime - this.IS.lastScanTime), this.PRE);
		this.DN				:= this.ACC >= this.PRE;
	ELSE // function is disabled, do not reset ACC unless explicitly reset from abRES
		this.DN				:= FALSE;
	END_IF
		
	// set outputs
	this.TT				:= this.EN AND NOT(this.DN);
	RTO					:= this.DN;
	this.IS.oldEnable	:= this.EN;
		
END_FUNCTION

(*Generates a symmetric pulse with period .PRE*)
FUNCTION PULSE
	
	this ACCESS pTimer;
	this.EN	:= enable; // use function input as this.EN to keep rung logic flow
	
	IF this.EN THEN
		IF NOT(this.IS.oldEnable) OR this.IS.reset THEN
			this.IS.reset		:= FALSE;
			this.IS.startTime	:= clock_ms();
		END_IF
		
		// set enabled outputs
		this.IS.currentTime	:= clock_ms();
		this.ACC			:= MIN(TIME_TO_DINT(this.IS.currentTime - this.IS.startTime), this.PRE);
		this.DN				:= this.ACC >= (this.PRE / 2);
		RES(this.ACC >= this.PRE , pTimer);
	ELSE // function is disabled, act as reset
		this.ACC	:= 0;
		this.DN		:= FALSE;
		this.TT		:= FALSE;
	END_IF
	
	// set outputs
	this.TT				:= this.EN; // timer is generating pulses constantly while enabled
	PULSE				:= this.DN;
	this.IS.oldEnable	:= this.EN;

END_FUNCTION

(* Resets an instance of TIMER_typ *)
FUNCTION RES
	
	this ACCESS pTimer;
	
	// clear timer function values and send reset
	IF enable THEN
		this.ACC		:= 0;
		this.DN			:= 0;
		this.TT			:= 0;
		this.IS.reset	:= TRUE;
	END_IF
	
	// pass signal through logic
	RES	:= enable;
	
END_FUNCTION



(* Resets an instance of TIMER_typ *)
FUNCTION REScounter
	
	this ACCESS pCounter;
	
	// clear timer function values and send reset
	IF Enable THEN
		this.ACC		:= 0;
		this.DN			:= 0;
		this.OV			:= 0;
		this.UN			:= 0;
	END_IF
	
	// pass signal through logic
	REScounter	:= Enable;
	
END_FUNCTION


FUNCTION_BLOCK TON_ab

	TON_FB.IN := EN;
	TON_FB.PT := DINT_TO_TIME(PRE);

	TON_FB();
	
	DN := TON_FB.Q;
	ACC := TIME_TO_DINT(TON_FB.ET);
	TT := TON_FB.IN AND NOT TON_FB.Q;
	
END_FUNCTION_BLOCK


FUNCTION_BLOCK TOF_ab

	TOF_FB.IN := EN;
	TOF_FB.PT := DINT_TO_TIME(PRE);

	TOF_FB();
	
	DN := TOF_FB.Q;
	ACC := TIME_TO_DINT(TOF_FB.ET);
	TT := TOF_FB.IN AND NOT TOF_FB.Q;
	
END_FUNCTION_BLOCK